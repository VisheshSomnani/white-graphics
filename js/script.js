/**
 * scripts.js
 * Contains Script for basic static website named "White Graphics"
 */

 $(window).on('load', function (){
     $('#preloader').delay(150).fadeOut('slow');
 });


 $(function () {
     $("#team-members").owlCarousel({
         items: 2,
         autoplay: true,
         smartSpeed: 700,
         loop: true,
         autoplayHoverPause: true,
         dots: false,
         nav: true,
         navText: ['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i>'],
         responsive:{
             0:{
                 items: 1
             }, 
             400:{
                 items:2
             }
         }
     });
     $("#cards").owlCarousel({
         items: 1,
         autoplay:false,
         smartSpeed: 700,
         loop: true,
         dots: false,
         nav: true,
         navText: ['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i>']
     });




     $("#progress-elements").waypoint(function (){
         $('.progress-bar').each(function (){
             $(this).animate({
                 width: $(this).attr('aria-valuenow') + "%"
             }, 800);
         });
         
         this.destroy();
     },{
         offset: 'bottom-in-view'
     }
    );
     
    $("#services-tabs").responsiveTabs({
        animation: 'slide'
    });
     
    $("#isotope-container").isotope({});
    $('#isotope-filters').on('click', 'button', function () {
        var filterValue = $(this).attr('data-filter');
        $("#isotope-container").isotope({
            filter: filterValue
        });

        $("#isotope-filters").find(".active").removeClass("active");
        $(this).addClass("active");
    });
    
     
    $("#portfolio-wrapper").magnificPopup({
        delegate: 'a', // child items selector, by clicking on it popup will open
        type: 'image',
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true
        },
        overflowY: 'scroll',
        callbacks:{
            open : function() {
                $("html").css('margin-right','0');
            },
        }
    });
     
    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });
    
    showHideNav();
    $(window).scroll(function(){
        showHideNav();
    });
    
    function showHideNav(){
        if($(window).scrollTop() >50){
            $("nav").addClass("scrolled-navbar green-nav-top");
            $(".navbar-brand img").attr('src','img/logo/logo-dark.png');
            $(".btn-back-to-top").slideDown();
        }else{
            $("nav").removeClass("scrolled-navbar green-nav-top");
            $(".navbar-brand img").attr('src','img/logo/logo.png');
            $(".btn-back-to-top").slideUp();
        }
    }
    
    $("#mobile-nav-open-btn").click(function(){
        $('#mobile-nav').css('height','100%');
        $(".mobile-nav-content").css('display','block');
        $("#mobile-nav-close-btn").css('display',"block");
    });
    $("#mobile-nav-close-btn").click(function(){
        $('#mobile-nav').css('height','0');
        $(".mobile-nav-content").css('display','none');
        $("#mobile-nav-close-btn").css('display',"none");
    });
    
    $(".smooth-scroll").click(function(e){
        e.preventDefault();
        var section_id = $(this).attr("href");
        $("html,body").animate({
            scrollTop: $(section_id).offset().top - 50
        },1250,"easeInOutExpo");
    });
     
    var addressString = "<h2>White Graphics</h2><hr><p>301 Evergreen CHS.,<br> Airoli, Maharashtra, India.</p>";
    var myLatLng = {
        lat: 19.145217,
        lng: 72.989140
    };
    
    var map = new google.maps.Map(document.getElementById("map"), {
        zoom: 13,
        center: myLatLng
    });
     
    var marker = new google.maps.Marker({
        position:myLatLng,
        map: map,
        title: 'click to see the address!'
    });
     var infoWindow = new google.maps.InfoWindow({
         content: addressString
     });
     marker.addListener('click',function(){
         infoWindow.open(map,marker);
     });
 });
//SERVICES SECTION
